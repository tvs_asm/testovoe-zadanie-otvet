<?php

namespace App\Observers;

use App\Jobs\InformCRMJob;
use App\Models\Lead;

class LeadObserver
{
    /**
     * Handle the Lead "created" event.
     * Это называется Аспектно ориентированное программироварие
     * мы подписываемся
     *
     * @param Lead $lead
     * @return void
     */
    public function created(Lead $lead)
    {
        if ($lead->wantToBay) {
            InformCRMJob::dispatch($lead);
        }
    }
}
