<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Models\Lead;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class CreateLeadMutation extends Mutation
{
    protected $attributes = [
        'name' => 'createLead',
        'description' => 'Create new Lead'
    ];

    public function type(): Type
    {
        return GraphQL::type('Lead');
    }

    public function args(): array
    {
        return [
            'name' => [
                'name' => 'name',
                'type' =>  Type::nonNull(Type::string()),
            ],
            'email' => [
                'name' => 'email',
                'type' =>  Type::nonNull(Type::string()),
            ],
            'phone' => [
                'name' => 'phone',
                'type' =>  Type::nonNull(Type::string()),
            ],
            'wantToBay' => [
                'name' => 'wantToBay',
                'type' =>  Type::nonNull(Type::boolean()),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $lead = new Lead();
        $lead->fill($args);
        $lead.save();

        return $lead;
    }
}
