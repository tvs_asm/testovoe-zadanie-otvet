<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Models\Lead;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class LeadType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Lead',
        'description' => 'A Lead',
		'model' => Lead::class
    ];

    public function fields(): array
    {
        return [
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Name',
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Email',
            ],
            'phone' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'phone',
            ],
            'wantToBay' => [
                'type' => Type::nonNull(Type::boolean()),
                'description' => 'wantToBay',
            ]
        ];
    }
}
