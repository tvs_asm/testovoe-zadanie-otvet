<?php

namespace App\GraphQL\Queries;

use App\Models\Lead;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class LeadQuery extends Query
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'Lead',
        'description' => 'Get one Lead'
    ];

    public function type(): Type
    {
        return GraphQL::type('Lead');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $id = (int) $args['id'];

        return Lead::query()->where('id', $id);
    }
}
