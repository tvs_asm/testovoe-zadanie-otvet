<?php

namespace App\GraphQL\Queries;

use App\Models\Lead;
use GraphQL\Type\Definition\Type;
use \Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class LeadsQuery extends Query
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'Leads',
        'description' => 'Get list of Leads'
    ];

    public function type(): Type
    {
        return GraphQL::paginate('Lead');
    }

    public function args(): array
    {
        return [
            'page' => [
                'type' => Type::int(),
                'defaultValue' => 1,
            ],
            'limit' => [
                'type' => Type::int(),
                'defaultValue' => 10,
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $page = (int) $args['page'];
        $limit = (int) $args['limit'];

        $query = Lead::query();

        return $query->paginate($limit, $page, ['*'], 'page');
    }
}
